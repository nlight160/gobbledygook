package model;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.Set;

/**
 * Basic Graph class where nodes are objects of generic type T which implements
 * Comparable<T> The set of edges is stored in a field of type Map(T,List<T>)
 * 
 * @author Nathaniel Lightholder, Tyler Vega
 * @version Fall 2018
 *
 */
public class WeightedDigraph<T extends Comparable<T>> {

	private Map<T, HashMap<T, Integer>> edges;

	/**
	 * Creates a new empty Digraph<T> object must add nodes and edges using the
	 * interface
	 * 
	 * @preconditions none
	 * @postconditions order() == 0
	 */
	public WeightedDigraph() {
		this.edges = new TreeMap<T, HashMap<T, Integer>>();
	}

	/**
	 * Returns the number of nodes in the graph
	 * 
	 * @return this.edges.KeySet().size()
	 */
	public int order() {
		return this.edges.keySet().size();
	}

	/**
	 * Return the set of nodes
	 * 
	 * @return this.edges.KeySet()
	 */
	public Set<T> nodeSet() {
		return this.edges.keySet();
	}

	/**
	 * Adds a new node to the graph Throws an exception if the node is already in
	 * the graph
	 * 
	 * @param node
	 * @preconditions node != null
	 * @postconditions order() = order()@prev + 1
	 */
	public void addNode(T node) {
		if (edges.containsKey(node)) {
			throw new IllegalArgumentException("Can not add duplicate node to graph");
		}
		edges.put(node, new HashMap<T, Integer>());
	}

	/**
	 * Adds a new node to the graph Throws an exception if the node is already in
	 * the graph
	 * 
	 * @param node
	 *            the node
	 * @param connections
	 *            the connections to the node
	 * @preconditions node != null
	 * @postconditions order() = order()@prev + 1
	 */
	public void addNodeWithConnections(T node, HashMap<T, Integer> connections) {
		if (edges.containsKey(node)) {
			throw new IllegalArgumentException("Can not add duplicate node to graph");
		}
		edges.put(node, connections);
	}

	/**
	 * Returns true if the node is in the graph and false otherwise
	 * 
	 * @param node
	 * @preconditions node != null
	 * @postconditions none
	 * @return this.edges.keySet().contains(node)
	 */
	public boolean containsNode(T node) {
		return this.edges.keySet().contains(node);
	}

	/**
	 * Adds an edge to the graph If either nodes are not already in the graph they
	 * are added
	 * 
	 * @param node0
	 * @param node1
	 */
	public void addEdge(T node0, T node1) {
		if (!containsNode(node0)) {
			addNode(node0);
		}
		if (!containsNode(node1)) {
			addNode(node1);
		}

		if (getNeighbors(node0).containsKey(node1)) {
			int count = this.edges.get(node0).get(node1);
			this.edges.get(node0).put(node1, count + 1);
		}
		this.edges.get(node0).put(node1, 1);
	}

	/**
	 * Returns true if the two nodes share an edge
	 * 
	 * @param node0
	 * @param node1
	 * @return
	 */
	public boolean adjacent(T node0, T node1) {
		if (!containsNode(node0) || !containsNode(node1)) {
			return false;
		}
		return edges.get(node0).containsKey(node1);
	}

	/**
	 * Returns the degree of a node
	 * 
	 * @param node
	 * @return the degree
	 */
	public int degree(T node) {
		if (!containsNode(node)) {
			throw new IllegalArgumentException("Node must be in graph to " + "compute degree");
		}
		return edges.get(node).size();
	}

	/**
	 * Returns the list of nodes adjacent to the input node
	 * 
	 * @param node
	 * @return the list of neighbors of the input node
	 */
	public Map<T, Integer> getNeighbors(T node) {
		if (!containsNode(node)) {
			throw new IllegalArgumentException("Node must be in graph to" + "provide neighbors");
		}
		return edges.get(node);
	}
	
	/**
	 * Returns the word count map inside edges
	 * 
	 * @param x the word whose map needs to be retrieved
	 * @return the wrod count map
	 */
	public Map<T, Integer> getWordCount(String x) {
		return this.edges.get(x); 
	}
	
}
