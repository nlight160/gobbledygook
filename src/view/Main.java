package view;

import java.io.IOException;
import java.util.Scanner;

import controller.Parser;

/**
 * Main Driver
 * @author Tyler Vega, Nathaniel Lightholder
 * @version Fall 2018
 */
public class Main {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		run(scan);
		boolean keepRunning = true;
		while(keepRunning == true) {
			System.out.println("Do you want to generate another?\nPlease type 'Yes' or 'No'...");
			String answer = scan.nextLine();
			if(answer.equalsIgnoreCase("Yes")) {
				run(scan);
			} else if(answer.equalsIgnoreCase("No")) {
				keepRunning = false;
			} else {
				System.out.println("Please type 'Yes' or 'No'...");
			}
		}
		System.out.println("Program Terminated");
		scan.close();

	}

	private static void run(Scanner scan) {
		boolean validInput = false;
		
		while (validInput == false) {

			System.out.print("Do you want to use the Sherlock Generator or White Fang Text Generator?\n"
					+ "Type 'Sherlock' or 'Fang'...\n");
			String generatorChoice = scan.nextLine();
			if (generatorChoice.equalsIgnoreCase("Sherlock")) {
				runGenerator("The_Adventures_of_Sherlock_Holmes.txt");
				validInput = true;
			} else if (generatorChoice.equalsIgnoreCase("Fang")) {
				runGenerator("White_Fang.txt");
				validInput = true;
			} else {
				System.out.println("Please enter one of the two choices.\n");
			}
		}
	}

	private static void runGenerator(String fileName) {
		Parser parser = new Parser();
		try {
			parser.populateDigraph(fileName);
		} catch (IOException e) {
			System.out.println("File was not found");
			e.printStackTrace();
		}
		StringBuilder sb = new StringBuilder();
		sb.append("the ");
		String nextWord = parser.getNext("the");
		sb.append(nextWord);
		for (int i = 1; i <= 200; i++) {
			nextWord = parser.getNext(nextWord);
			if (nextWord.equals(".")) {
				nextWord = parser.getNext(nextWord);
				sb.append(nextWord);
			} else {
				sb.append(" " + nextWord);
			}

			if (i % 20 == 0) {
				sb.append("\n");
			}

		}
		System.out.println(sb);
	}

}
