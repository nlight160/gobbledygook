package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map.Entry;

import model.WeightedDigraph;

/**
 * Parser for the text generator
 * 
 * @author Tyler Vega, Nathaniel Lightholder
 * @version Fall 2018
 */
public class Parser {

	private WeightedDigraph<String> digraph;

	/**
	 * Handles loading the digraph and producing gobbledygook text
	 * 
	 * @precondition none
	 * @postcondition parser is created
	 */
	public Parser() {
		this.digraph = new WeightedDigraph<String>();
	}

	/**
	 * populates the digraph with text from the specified file
	 * 
	 * @postcondition digraph is loaded
	 * 
	 * @param fileName the text file to be loaded
	 * @throws IOException if file can not be read or found
	 */
	public void populateDigraph(String fileName) throws IOException {
		String current = "";
		String previous = "";
		File book = new File(fileName);
		BufferedReader reader = new BufferedReader(new FileReader(book));
		while ((current = reader.readLine()) != null) {
				current = this.removeNonLetters(current);
				String[] split = current.split(" ");
				for (int i = 0; i < split.length; i++) {
					if (!this.digraph.containsNode(split[i])) {
						this.digraph.addNode(split[i]);
					}
					if (!previous.isEmpty() && previous != null) {
						this.digraph.addEdge(previous, split[i]);
					}
					previous = split[i];
				}
		}
		reader.close();
	}

	private String removeNonLetters(String line) {
		String plain = line.replaceAll("[^a-zA-Z\'\\s\\.]", "").toLowerCase();
		return plain;
	}

	/**
	 * Takes a string in the digraph and gives the next
	 * gobbledygook word
	 * 
	 * @param x the word used to find the next word
	 * @return the next word in the gobbledygook
	 */
	public String getNext(String x) {
		int acc = 0;
		int n = this.digraph.degree(x);
		int dart = (int) (n * Math.random());
		for (Entry<String, Integer> entry : this.digraph.getWordCount(x).entrySet()) {
			acc += entry.getValue();
			if (acc >= dart) {
				return entry.getKey();
			}
		}
		return ".";
	}

}
